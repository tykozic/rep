# rep

a terminal spaced repetition system based on parametric tags.

## usage

the spacing algorithm is inspired by <https://github.com/jsageryd/gaku>. knowing a card buries it twice as far down in the stack as it was. forgetting one buries it half as far.

invoke `rep` to be prompted with the top card of the deck. simply follow the prompts.

## TODO list

* multiple decks (maybe. I use only one, so demand is low)
* adding decks via command line

## card files

`<` and `>` delimit placeholder descriptions of file contents. the lines with only backslashes are literal.

file: `<any-filename-you-want>`
```
<card prompt (may be multiline)>
\
<card response (may be multiline)>
\
<card tags (must be at most one line)>
```

tags are listed separated by colons. whitespace is ignored. parametric tags may include exactly one pair "(.*)" of parentheses, between which is the parameter. whitespace is ignored and therefore cannot be considered part of the tag.

```
:language:french:due(15d):
```

## the QUEUE file

`rep` expects a directory `$XDG_DATA_HOME/rep` to exist and include a file called `QUEUE`. its format is simple. each line must contain two fields separated by a tab. the first is a path to a card file relative to `$XDG_DATA_HOME/rep`. the second is the amount by which that card was last moved. this file must be populated manually, but `rep` manages the 'last moved' field on its own. this file can be changed at will to change the order and 'last moved' parameters of cards.

every time a card is requested for study, the first line of the queue is examined. depending on whether the card is known or not, the line will be moved more or less far down the queue than it was previously. see `files.ha` for implementation details.
